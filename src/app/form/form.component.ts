import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Endereco } from './form.model';
import { Subscription } from 'rxjs';
import { FormService } from './services/form.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy {
  enderecoForm: FormGroup;
  enderecoSubscription: Subscription;

  constructor(
    private formService: FormService,
  ) { }

  ngOnInit() {
    this.enderecoForm = new FormGroup({
      rua: new FormControl(''),
      numero: new FormControl(''),
      bairro: new FormControl(''),
      cidade: new FormControl(''),
      estado: new FormControl(''),
      pais: new FormControl(''),
      cep: new FormControl('')
    });

    this.enderecoSubscription = this.formService.getEnderecoAsync().subscribe(
      (endereco: Endereco) => {
        this.enderecoForm.setValue(endereco);
        console.log(endereco);
      }
    );

    this.formService.getEnderecoFromApi();
  }

  ngOnDestroy() {
    this.enderecoSubscription.unsubscribe();
  }

  save() {
    this.formService.saveEndereco(this.enderecoForm.value).subscribe(
      () => console.log('saved'),
      (err) => console.log(err)
    );
  }

}

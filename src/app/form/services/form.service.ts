import { Injectable } from '@angular/core';
import { GenericStoreService } from './generic-store.service';
import { Endereco } from '../form.model';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { tap, filter } from 'rxjs/operators';

@Injectable()
export class FormService extends GenericStoreService<Endereco> {
  constructor(private apiService: ApiService) {
    super();
  }

  getEnderecoAsync(): Observable<Endereco> {
    return this.getAsync()
      .pipe(filter(value => !!value));
  }

  getEnderecoSync(): Endereco {
    return this.getSync();
  }

  getEnderecoFromApi() {
    this.apiService
      .getEnderecoFromApi()
      .subscribe((result: Endereco) => this.setEndereco(result));
  }

  setEndereco(endereco: Endereco) {
    this.set(endereco);
  }

  saveEndereco(endereco: Endereco): Observable<Endereco> {
    return this.apiService
      .saveEnderecoToApi(endereco)
      .pipe(tap(() => this.setEndereco(endereco)));
  }
}

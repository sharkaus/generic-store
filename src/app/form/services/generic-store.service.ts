import { Observable, BehaviorSubject } from 'rxjs';

export abstract class GenericStoreService<T> {
  private model: T;
  private modelSubject = new BehaviorSubject<T>(this.model);

  constructor() {}

  protected getAsync(): Observable<T> {
    return this.modelSubject.asObservable();
  }

  protected getSync(): T {
    return this.modelSubject.getValue();
  }

  protected set(payload: T) {
    this.model = Object.assign({}, payload);
    this.modelSubject.next(this.model);
  }
}

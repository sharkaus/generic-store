import { Endereco } from '../form.model';
import { Observable, Subject } from 'rxjs';

export class ApiService {

  constructor() {}

  getEnderecoFromApi(): Observable<Endereco> {
    const getSubject = new Subject<any>();

    // makes request to API
    setTimeout(() => {
      const endereco: Endereco = {
      rua: 'Rua 1',
      bairro: 'Jardim 1',
      cep: '11545-000',
      cidade: 'Sorocaba',
      numero: 123,
      pais: 'Brazil',
      estado: 'SP'
      };

      getSubject.next(endereco);
      getSubject.complete();
    }, 1000);
    return getSubject.asObservable();
  }

  saveEnderecoToApi(endereco: Endereco): Observable<Endereco> {
    const setSubject = new Subject<any>();
    // makes request to API
    setTimeout(() => {
      setSubject.next(endereco);
      setSubject.complete();
    }, 1000);

    // setTimeout(() => {
    //   setSubject.error('request error');
    // }, 1000);

    return setSubject.asObservable();
  }

}

export interface Endereco {
  rua: string;
  numero: number;
  bairro: string;
  cidade: string;
  estado: string;
  pais: string;
  cep: string;
}
